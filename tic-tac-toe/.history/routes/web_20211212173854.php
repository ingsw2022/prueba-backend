<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*Route::get('/register', function () {
    return view('nueva_partida.register');
});*/


Route::get('/newgame', function () {
    return view('nueva_partida.new-game');
});

Route::get('/unirseapartida', function () {
    return view('unirse_a_partida.current-game');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/new-game', 'HomeController@newGame');

Route::get('/board/{id}', 'GameController@board');

Route::post('/play/{id}', 'GameController@play');

Route::post('/game-over/{id}', 'GameController@gameOver');

